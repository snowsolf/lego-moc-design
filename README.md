# LEGO MOC Design

这是LEGO MOC的设计模型和搭建图纸，模型设计绝大部分来源于网络，非原创。

文件说明：

- xxx.io - Studio格式的模型
- xxx.ldr - LDraw格式的模型
- xxx.pdf - 自动生成的搭建图纸
- xxx.png - 渲染效果图
